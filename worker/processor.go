package worker

import (
	"github.com/gocraft/work"

	"dzik/jobs"
	"dzik/pusher"
)

type processor struct {
	pu pusher.Service
}

func NewProcessor(pu pusher.Service) jobs.Processor {
	return &processor{pu: pu}
}

func (p *processor) PushOrder(job *work.Job) error {
	pop, err := jobs.NewPushOrderParamsFromJob(job)
	if err != nil {
		panic(err)
	}
	p.pu.PushOrder(pop.ShuttleID, pop.OrderID, pop.Passengers)

	return nil
}
