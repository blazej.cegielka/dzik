build: setup build-api build-worker

setup:
	mkdir -p build

build-api:
	go build -o build/api cmd/api/main.go

build-worker:
	go build -o build/worker cmd/worker/main.go

test:
	go test -v ./...

test-coverage:
	go test ./... -coverprofile=coverage.out

test-coverage-report:
	go tool cover -html=coverage.out

lint:
	golangci-lint run --no-config --issues-exit-code=0 --deadline=30m \
		--disable-all --enable=deadcode  --enable=gocyclo --enable=golint --enable=varcheck \
		--enable=structcheck --enable=maligned --enable=errcheck --enable=dupl --enable=ineffassign \
		--enable=interfacer --enable=unconvert --enable=goconst --enable=gosec --enable=megacheck

docker-build: docker-build-api docker-build-worker

docker-build-api:
	docker build -f build/api.Dockerfile -t dzik-api ./build/

docker-build-worker:
	docker build -f build/worker.Dockerfile -t dzik-worker ./build/

docker-run:
	docker-compose -f build/docker-compose.yml up

clean:
	rm -r build
