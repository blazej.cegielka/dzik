package main

import (
	"flag"
	"log"

	"dzik/config"
	"dzik/db"
	"dzik/dispatcher"
	"dzik/jobs"
	"dzik/planner"
	"dzik/pusher"
	"dzik/server"
)

func main() {
	s := config.NewConfig()

	createDB := flag.Bool("create-db", false, "create db using schema.sql file")
	sampleDB := flag.Bool("sample-db", false, "add sample db data")
	flag.Parse()

	db := db.Connect(s.DBName)

	if *createDB {
		db.Create(s.DBSchema)
	}
	if *sampleDB {
		db.Sample()
	}

	jobsStore := jobs.NewRedisPool(s.RedisHost, s.RedisPort)
	log.Println(s)

	je := jobs.NewEnqueuer(s.JobsNamespace, jobsStore)

	pl := planner.NewService(db, je)
	pu := pusher.NewService(db, je)
	di := dispatcher.NewService(db, je)

	h := server.NewHandler(pl, pu, di)
	srv := server.NewServer(s.ServerPort, h)
	srv.Run()
}
