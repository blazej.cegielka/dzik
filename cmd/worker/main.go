package main

import (
	"log"
	"os"
	"os/signal"

	"dzik/config"
	"dzik/db"
	"dzik/jobs"
	"dzik/pusher"
	"dzik/worker"
)

func main() {
	s := config.NewConfig()
	log.Println(s)

	db := db.Connect(s.DBName)
	jobsStore := jobs.NewRedisPool(s.RedisHost, s.RedisPort)

	je := jobs.NewEnqueuer(s.JobsNamespace, jobsStore)
	pu := pusher.NewService(db, je)

	p := worker.NewProcessor(pu)
	worker := jobs.NewWorker(s.JobsNamespace, jobsStore, p)

	// Start processing jobs
	worker.Start()

	// Wait for a signal to quit:
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, os.Kill)
	<-signalChan

	// Stop the pool
	worker.Stop()
}
