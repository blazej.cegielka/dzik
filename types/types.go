package types

import (
	"fmt"

	"github.com/google/uuid"
)

type Location struct {
	ID       uuid.UUID `json:"id"`
	Name     string    `json:"name"`
	Disabled bool      `json:"disabled"`
}

type Link struct {
	ID     uuid.UUID `json:"id"`
	FromID uuid.UUID `json:"from_id" db:"from_id"`
	ToID   uuid.UUID `json:"to_id" db:"to_id"`
}

type Event struct {
	ID         uuid.UUID `json:"id"`
	LocationID uuid.UUID `json:"location_id" db:"location_id"`
	Time       string    `json:"time"`
}

type Shuttle struct {
	ID        uuid.UUID `json:"id" db:"id"`
	FromID    uuid.UUID `json:"from_id" db:"from_id"`
	ToID      uuid.UUID `json:"to_id" db:"to_id"`
	Date      string    `json:"date" db:"date"`
	Departure string    `json:"departure" db:"departure"`
	// Arrival    string    `json:"arrival"`
	// Price      string    `json:"price"`
	Popularity int    `json:"popularity"`
	FromName   string `json:"from_name" db:"from_name"`
	ToName     string `json:"to_name" db:"to_name"`
}

func NewShuttle(fromID uuid.UUID, toID uuid.UUID, date string, time string) Shuttle {
	s := Shuttle{FromID: fromID, ToID: toID, Date: date, Departure: time}
	s.ID = NewUUIDFromShuttle(s)
	return s
}

type Order struct {
	ID               uuid.UUID  `json:"id" db:"id"`
	UserID           string     `json:"user_id" db:"user_id"`
	ShuttleID        uuid.UUID  `json:"shuttle_id" db:"shuttle_id"`
	ShuttleFrom      string     `json:"shuttle_from" db:"shuttle_from"`
	ShuttleTo        string     `json:"shuttle_to" db:"shuttle_to"`
	ShuttleDate      string     `json:"shuttle_date" db:"shuttle_date"`
	ShuttleDeparture string     `json:"shuttle_departure" db:"shuttle_departure"`
	Shuttle          *Shuttle   `json:"shuttle"`
	RideID           *uuid.UUID `json:"ride_id" db:"ride_id"`
	Ride             *Ride      `json:"ride"`
	Passengers       int        `json:"passengers"`
	Split            int        `json:"split" db:"split"`
}

func NewOrder(userID string, s Shuttle, passengers int, split int) *Order {
	o := &Order{
		ID:               uuid.New(),
		UserID:           userID,
		ShuttleID:        s.ID,
		ShuttleFrom:      s.FromName,
		ShuttleTo:        s.ToName,
		ShuttleDate:      s.Date,
		ShuttleDeparture: s.Departure,
		Shuttle:          &s,
		Passengers:       passengers,
		Split:            split,
	}
	return o
}

type Ride struct {
	ID         uuid.UUID `json:"id" db:"id"`
	ShuttleID  uuid.UUID `json:"shuttle_id" db:"shuttle_id"`
	Shuttle    *Shuttle
	Orders     *[]Order
	Capacity   int
	Passengers int
}

func NewRide(s Shuttle, c int) *Ride {
	r := &Ride{
		ID:        uuid.New(),
		ShuttleID: s.ID,
		Shuttle:   &s,
		Capacity:  c,
	}
	return r
}

func NewUUID(v []byte) uuid.UUID {
	return uuid.NewSHA1(uuid.Nil, v)
}

func NewUUIDShuttleKey(from uuid.UUID, to uuid.UUID, date string, time string) []byte {
	return []byte(fmt.Sprintf("%d|%d|%s|%s", from, to, date, time))
}

func NewUUIDFromShuttle(s Shuttle) uuid.UUID {
	key := NewUUIDShuttleKey(s.FromID, s.ToID, s.Date, s.Departure)
	return NewUUID(key)
}
