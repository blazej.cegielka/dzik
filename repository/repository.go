package repository

import (
	"github.com/google/uuid"

	"dzik/types"
)

type Repository interface {
	GetLocations() []types.Location
	GetLocationByID(locationID uuid.UUID) (types.Location, bool)
	GetLocationLinks(locationID uuid.UUID) ([]types.Link, bool)
	GetLinksIDMap() map[uuid.UUID][]uuid.UUID
	GetEvents() []types.Event

	GetShuttleByIDWithOrders(shuttleID uuid.UUID) (types.Shuttle, bool)
	GetShuttleByID(shuttleID uuid.UUID) (types.Shuttle, bool)
	CreateShuttle(types.Shuttle)

	GetOrders(userID string) []types.Order
	CreateOrder(types.Order)
	UpdateOrderRide(orderID uuid.UUID, rideID uuid.UUID)
	GetAvailableRideForShuttle(shuttleID uuid.UUID, space int) (*types.Ride, error)

	GetOrderShuttlesWithoutRide() (shuttleIDs []uuid.UUID, err error)
	GetOrdersWithoutRideByShuttleID(shuttleID uuid.UUID) ([]types.Order, error)

	CreateRide(types.Ride)
}
