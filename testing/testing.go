package test

import (
	"os"

	"github.com/gomodule/redigo/redis"

	"dzik/config"
	dbpkg "dzik/db"
	"dzik/jobs"
)

var (
	Config    config.Specification
	DB        *dbpkg.Client
	JobsStore *redis.Pool
	JE        jobs.Enqueuer
)

func NewTestConfig() config.Specification {
	os.Setenv("MRUK_DB_NAME", "test_foo")
	os.Setenv("MRUK_DB_SCHEMA", "../db/schema.sql")
	os.Setenv("MRUK_JOBS_NAMESPACE", "test_orders")

	return config.NewConfig()
}

func TestMain() func() {
	Config = NewTestConfig()

	DB = dbpkg.Connect(Config.DBName)
	DB.Create(Config.DBSchema)

	JobsStore = jobs.NewRedisPool(Config.RedisHost, Config.RedisPort)
	jobs.MustPing(JobsStore)

	// je := jobs.NewEnqueuer(jobsNs, jobsStore)
	JE = jobs.NewEnqueuerMock()

	return TestMainDefer
}

func TestMainDefer() {
	dbpkg.Drop(Config.DBName)
	jobs.MustFlushDB(JobsStore)
}

func Setup() func() {
	DB.Savepoint("test")
	JE.(*jobs.EnqueuerMock).Calls = nil

	return Teardown
}

func Teardown() {
	defer DB.Rollback("test")
}
