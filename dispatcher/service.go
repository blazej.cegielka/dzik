package dispatcher

import (
	"log"

	"github.com/google/uuid"

	"dzik/jobs"
	"dzik/repository"
	"dzik/types"
	"dzik/utils"
)

// Service provides dispatching operations.
type Service interface {
	ListPackedOrders() *openOrders
	AddRide(s *types.Shuttle, capacity int)
}

// service is a concrete implementation of SearchService
type service struct {
	db repository.Repository
	je jobs.Enqueuer
}

func NewService(db repository.Repository, je jobs.Enqueuer) Service {
	return &service{
		db: db,
		je: je,
	}
}

type details struct {
	Orders []types.Order `json:"orders"`
	From   string        `json:"from"`
	To     string        `json:"to"`
}

// date/departure/shuttleid
type openOrders map[string]map[string]map[uuid.UUID]details

// ListOpenOrders list packed orders.
func (sv *service) ListPackedOrders() *openOrders {
	oo := openOrders{}
	sIDs, _ := sv.db.GetOrderShuttlesWithoutRide()

	for _, sID := range sIDs {
		os, err := sv.db.GetOrdersWithoutRideByShuttleID(sID)
		if err != nil {
			log.Printf("Error getting orders without ride: %s", err)
		}
		pos := utils.PackOrders(os)

		for _, po := range pos {
			for _, o := range po.Orders {
				log.Printf("order %v %v %v", o.ShuttleDate, o.ShuttleDeparture, o.ShuttleID)

				l1, ok := oo[o.ShuttleDate]
				if !ok {
					l1 = map[string]map[uuid.UUID]details{}
					oo[o.ShuttleDate] = l1
				}
				l2, ok := l1[o.ShuttleDeparture]
				if !ok {
					l2 = map[uuid.UUID]details{}
					l1[o.ShuttleDeparture] = l2
				}
				l3, ok := l2[o.ShuttleID]
				if !ok {
					l3 = details{From: o.ShuttleFrom, To: o.ShuttleTo, Orders: []types.Order{o}}
				} else {
					l3.Orders = append(l3.Orders, o)
				}
				l2[o.ShuttleID] = l3
			}
		}
	}

	return &oo
}

func (sv *service) AddRide(s *types.Shuttle, capacity int) {
	r := types.NewRide(*s, capacity)
	sv.db.CreateRide(*r)
	sv.je.Enqueue(jobs.JobPushRide, jobs.Params{"who": "dispatcher"})
}
