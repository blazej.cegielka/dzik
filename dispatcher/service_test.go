package dispatcher

import (
	"os"
	"testing"

	"dzik/jobs"
	ltesting "dzik/testing"
	"dzik/types"
)

var (
	sv Service
)

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	defer ltesting.TestMain()()
	sv = NewService(ltesting.DB, ltesting.JE)

	return m.Run()
}

func TestAddRide(t *testing.T) {
	defer ltesting.Setup()()

	s := types.Shuttle{}
	ltesting.DB.CreateShuttle(s)

	capacity := 5
	sv.AddRide(&s, capacity)
	calls := ltesting.JE.(*jobs.EnqueuerMock).Calls
	expJobsNo := 1
	if len(calls) != expJobsNo {
		t.Errorf("Expects new jobs: %d, got %d", expJobsNo, len(calls))
	}

	expJobName := jobs.JobPushRide
	if calls[0].Name != expJobName {
		t.Errorf("Expects job.name: %s, got %s", expJobName, calls[0].Name)
	}

	expJobParams := "dispatcher"
	if calls[0].Params["who"] != expJobParams {
		t.Errorf("Expects job.params: %s, got %s", expJobParams, calls[0].Params)
	}

	r, _ := ltesting.DB.GetAvailableRideForShuttle(s.ID, 1)
	if r.Passengers != 0 {
		t.Errorf("Expects no passengers for new ride")
	}

	if r.Capacity != capacity {
		t.Errorf("Expects capacity %d, got %d", capacity, r.Capacity)
	}
}

func TestListPackedOrders(t *testing.T) {
	defer ltesting.Setup()()

	{
		os := sv.ListPackedOrders()
		got := len(*os)
		if got != 0 {
			t.Errorf("Expects no packed orders, got %d", got)
		}
	}

	{
		s := types.Shuttle{}
		ltesting.DB.CreateShuttle(s)
		o := types.Order{}
		ltesting.DB.CreateOrder(o)
		// r := types.Ride{}
		// db.UpdateOrderRide(o.ID, r.ID)

		os := sv.ListPackedOrders()
		got := len(*os)
		if got != 1 {
			t.Errorf("Expects no packed orders, got %d", got)
		}
	}
}
