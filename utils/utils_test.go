package utils

import (
	"fmt"
	"testing"

	"github.com/google/uuid"

	"dzik/types"
)

func newOrder(ono int) types.Order {
	id := fmt.Sprintf("00000000-0000-0000-0000-%012x", ono)
	return types.Order{ID: uuid.MustParse(id)}
}

func newOrderFromSplit(ono int, split int) types.Order {
	id := fmt.Sprintf("00000000-0000-0000-0000-%012x", ono)
	return types.Order{ID: uuid.MustParse(id), Split: split}
}

func newOrders(onos ...int) []types.Order {
	os := []types.Order{}

	for _, ono := range onos {
		os = append(os, newOrder(ono))
	}

	return os
}

func newOrdersFromSplit(splits ...int) []types.Order {
	os := []types.Order{}

	for i, split := range splits {
		os = append(os, newOrderFromSplit(i, split))
	}

	return os
}

func TestPackOrders(t *testing.T) {
	type packCounts map[int]int
	type subTest struct {
		orders   []types.Order
		expected map[int]int
	}

	sts := []subTest{
		{newOrdersFromSplit(), packCounts{}},
		{newOrdersFromSplit(1), packCounts{1: 1}},
		{newOrdersFromSplit(2), packCounts{}},
		{newOrdersFromSplit(2, 2), packCounts{2: 2}},
		{newOrdersFromSplit(2, 2, 2), packCounts{2: 3}},
		{newOrdersFromSplit(2, 3, 4, 5), packCounts{}},
		{newOrdersFromSplit(2, 2, 3, 3, 4, 5), packCounts{2: 2, 3: 2, 4: 1, 5: 1}},
	}

	for _, st := range sts {
		t.Run(fmt.Sprintf("%v", st.orders), func(t *testing.T) {
			got := PackOrders(st.orders)
			if len(got) != len(st.expected) {
				t.Errorf("Packs number expected: %d, got %d", len(st.expected), len(got))
			}
			for _, pack := range got {
				if len(pack.Orders) != st.expected[pack.Split] {
					t.Errorf("Orders pack split `%d` expected: %d, got %d", pack.Split, st.expected[pack.Split], len(pack.Orders))
				}
			}
			fmt.Println("Got: ", got)
		})
	}
}

func TestAssignRideToPack(t *testing.T) {
	type subTest struct {
		pos      []PackedOrder
		capacity int
		out      []types.Order
	}

	sts := []subTest{
		{
			pos:      []PackedOrder{},
			capacity: 5,
			out:      newOrders(),
		},
		{
			pos: []PackedOrder{
				PackedOrder{2, newOrders(1, 2)},
				PackedOrder{3, newOrders(3)},
				PackedOrder{4, newOrders(4)},
			},
			capacity: 5,
			out:      newOrders(1, 2, 3, 4),
		},
		{
			pos: []PackedOrder{
				PackedOrder{2, newOrders(1, 2)},
				PackedOrder{3, newOrders(3)},
				PackedOrder{4, newOrders(4)},
			},
			capacity: 3,
			out:      newOrders(1, 2, 3),
		},
	}

	for _, st := range sts {
		t.Run(fmt.Sprintf("%v", st.pos), func(t *testing.T) {
			os := AssignRideToPack(st.pos, st.capacity)
			fmt.Println("assigned orders", os)
			if len(os) != len(st.out) {
				t.Errorf("Assigned Orders length expected: %d, got %d", len(st.out), len(os))
			}
			for i, o := range os {
				if o != st.out[i] {
					t.Errorf("Assigned Orders expected: %d, got %d", st.out[i].ID, o.ID)
				}
			}
		})
	}

}
