package utils

import (
	"log"
	"sort"

	"dzik/types"
)

type PackedOrder struct {
	Split  int
	Orders []types.Order
}

func PackOrders(os []types.Order) (pos []PackedOrder) {
	if len(os) == 0 {
		return
	}

	ordersBySplit := map[int][]types.Order{}
	for _, o := range os {
		ordersBySplit[o.Split] = append(ordersBySplit[o.Split], o)
	}
	log.Printf("ordersBySplit %v", ordersBySplit)

	splits := []int{}
	for k := range ordersBySplit {
		splits = append(splits, k)
	}
	sort.Ints(splits)
	// sort.Sort(sort.Reverse(sort.IntSlice(splits)))
	log.Printf("splits %v", splits)

	splitCounts := [][]int{
		[]int{splits[0], len(ordersBySplit[splits[0]])},
	}
	if len(splits) > 1 {
		for i, s := range splits[1:] {
			log.Printf("splitCounts%v", splitCounts)
			log.Printf("iter %v %v %v", i, s, splits)
			splitCounts = append(splitCounts, []int{s, len(ordersBySplit[s]) + splitCounts[i][1]})
		}
	}

	log.Printf("splitCounts%v", splitCounts)

	for i := len(splitCounts) - 1; i >= 0; i-- {
		split, cnt := splitCounts[i][0], splitCounts[i][1]
		if split <= cnt {
			log.Printf("ready %v %v", split, cnt)
			pos = append(pos, PackedOrder{split, ordersBySplit[split]})
		}
	}

	log.Printf("pos %v", pos)
	return
}

func AssignRideToPack(pos []PackedOrder, capacity int) (os []types.Order) {
	for _, po := range pos {
		split, orders := po.Split, po.Orders

		if split > capacity {
			continue
		} else {
			toAllocate := capacity - len(os)
			if len(orders) >= toAllocate {
				os = append(os, orders[:toAllocate]...)
				break
			} else {
				os = append(os, orders...)
			}
		}
	}
	return
}
