package db

import (
	"database/sql"
	"log"

	"github.com/google/uuid"

	"dzik/types"
)

func (c *Client) GetShuttleByID(ID uuid.UUID) (types.Shuttle, bool) {
	var s types.Shuttle
	err := c.db.Get(&s, "SELECT * FROM shuttles WHERE id = ?", ID)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println("Error getting shuttles:", err, ID)
		}
		return s, false
	}

	return s, true
}

func (c *Client) GetShuttleByIDWithOrders(ID uuid.UUID) (types.Shuttle, bool) {
	var s types.Shuttle

	err := c.db.Get(&s, "SELECT s.*, COUNT(*) as popularity, lfrom.name from_name, lto.name to_name FROM shuttles s JOIN orders o ON s.id = o.shuttle_id JOIN locations lfrom ON s.from_id = lfrom.id JOIN locations lto ON s.to_id = lto.id WHERE s.id = ? GROUP BY s.id", ID)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println("Error getting shuttles:", err, ID)
		}
		return s, false
	}

	return s, true
}

func (c *Client) CreateShuttle(s types.Shuttle) {
	sql := `INSERT INTO shuttles(id, from_id, to_id, date, departure) VALUES (?, ?, ?, ?, ?)`
	c.db.MustExec(sql, s.ID, s.FromID, s.ToID, s.Date, s.Departure)
}
