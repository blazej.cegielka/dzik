package db

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3" // driver

	"dzik/types"
)

type Client struct {
	db     *sqlx.DB
	dbName string
}

func Connect(dbn string) *Client {
	db, err := sqlx.Open("sqlite3", fmt.Sprintf("./%s.db", dbn))
	if err != nil {
		panic(err)
	}
	return &Client{db, dbn}
}

func Drop(dbn string) {
	var filename string
	// fmt.Printf("dropping %s\n", dbn)

	filename = fmt.Sprintf("./%s.db", dbn)
	if err := os.Remove(filename); err != nil && !os.IsNotExist(err) {
		panic(err)
	}

	filename = fmt.Sprintf("./%s.db-journal", dbn)
	if err := os.Remove(filename); err != nil && !os.IsNotExist(err) {
		panic(err)
	}
}

func (c *Client) Create(schema string) {
	fd, err := os.Open(schema)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	rd := bufio.NewReader(fd)
	for {
		line, err := rd.ReadString(';')
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		c.db.MustExec(line)
	}
}

func (c *Client) Close() {
	c.db.Close()
}

func (c *Client) Savepoint(name string) {
	if _, err := c.db.Exec(fmt.Sprintf("SAVEPOINT %s;", name)); err != nil {
		panic(err)
	}
}

func (c *Client) Rollback(name string) {
	if _, err := c.db.Exec(fmt.Sprintf("ROLLBACK TO %s;", name)); err != nil {
		panic(err)
	}
}

func (c *Client) Sample() {
	shuttle := `INSERT INTO shuttles(id, from_id, to_id, date, departure) VALUES (?, ?, ?, ?, ?)`
	shuttles := []types.Shuttle{
		types.NewShuttle(uuid.MustParse("00000000-0000-0000-0000-000000000000"), uuid.MustParse("00000000-0000-0000-0000-000000000002"), "2019-05-01", "10:35"),
		types.NewShuttle(uuid.MustParse("00000000-0000-0000-0000-000000000000"), uuid.MustParse("00000000-0000-0000-0000-000000000002"), "2019-05-01", "14:35"),
		types.NewShuttle(uuid.MustParse("00000000-0000-0000-0000-000000000000"), uuid.MustParse("00000000-0000-0000-0000-000000000002"), "2019-05-01", "16:35"),
	}
	for _, s := range shuttles {
		c.db.MustExec(shuttle, s.ID, s.FromID, s.ToID, s.Date, s.Departure)
	}

	order := `INSERT INTO orders(id, user_id, shuttle_id, split) VALUES (?, ?, ?, ?)`
	c.db.MustExec(order, "00000000-0000-0000-0000-400000000000", "blazej.cegielka@gmail.com", shuttles[0].ID, 2)
	c.db.MustExec(order, "00000000-0000-0000-0000-400000000001", "blazej.cegielka@gmail.com", shuttles[0].ID, 3)
	c.db.MustExec(order, "00000000-0000-0000-0000-400000000002", "blazej.cegielka@gmail.com", shuttles[0].ID, 4)
	c.db.MustExec(order, "00000000-0000-0000-0000-400000000003", "blazej.cegielka@gmail.com", shuttles[0].ID, 5)
	c.db.MustExec(order, "00000000-0000-0000-0000-400000000004", "blazej.cegielka@gmail.com", shuttles[2].ID, 2)
}
