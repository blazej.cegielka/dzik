package db

import (
	// "fmt"
	// "log"

	"dzik/types"

	"github.com/google/uuid"
)

func (c *Client) GetOrders(userID string) []types.Order {
	var os []types.Order
	err := c.db.Select(&os, "SELECT o.*, lfrom.name shuttle_from, lto.name shuttle_to, s.date shuttle_date, s.departure shuttle_departure FROM orders o JOIN shuttles s ON o.shuttle_id = s.id JOIN locations lfrom ON s.from_id = lfrom.id JOIN locations lto ON s.to_id = lto.id WHERE user_id = ?", userID)
	if err != nil {
		panic(err)
	}

	return os
}

func (c *Client) GetOrdersWithoutRideByShuttleID(sID uuid.UUID) (os []types.Order, err error) {
	err = c.db.Select(&os, "SELECT o.*, lfrom.name shuttle_from, lto.name shuttle_to, s.date shuttle_date, s.departure shuttle_departure FROM orders o JOIN shuttles s ON o.shuttle_id = s.id JOIN locations lfrom ON s.from_id = lfrom.id JOIN locations lto ON s.to_id = lto.id WHERE shuttle_id = ? AND ride_id IS NULL", sID)
	return
}

func (c *Client) GetOrderShuttlesWithoutRide() (sIDs []uuid.UUID, err error) {
	err = c.db.Select(&sIDs, "SELECT o.shuttle_id FROM orders o WHERE ride_id IS NULL GROUP BY o.shuttle_id")
	return
}

func (c *Client) getOrdersForScheduleCount(sID uuid.UUID) int {
	var cnt int
	err := c.db.Select(&cnt, "SELECT COUNT(*) FROM orders WHERE shuttle_id = ?", sID)
	if err != nil {
		panic(err)
	}

	return cnt
}

func (c *Client) CreateOrder(o types.Order) {
	sql := `INSERT INTO orders(id, user_id, shuttle_id, split) VALUES (?, ?, ?, ?)`
	c.db.MustExec(sql, o.ID, o.UserID, o.ShuttleID, o.Split)
}

func (c *Client) UpdateOrderRide(oID uuid.UUID, rID uuid.UUID) {
	sql := `UPDATE orders SET ride_id = ? WHERE id = ?`
	c.db.MustExec(sql, rID, oID)
}
