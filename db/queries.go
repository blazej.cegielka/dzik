package db

import (
	"github.com/google/uuid"

	"dzik/types"
)

func (c *Client) GetLocations() []types.Location {
	var ls []types.Location
	err := c.db.Select(&ls, "SELECT * FROM locations")
	if err != nil {
		panic(err)
	}

	return ls
}

func (c *Client) GetLocationByID(ID uuid.UUID) (types.Location, bool) {
	var l types.Location
	err := c.db.Get(&l, "SELECT * FROM locations WHERE id = ?", ID)
	if err != nil {
		panic(err)
		return l, false
	}
	return l, true
}

func (c *Client) GetLocationLinks(ID uuid.UUID) ([]types.Link, bool) {
	var lxs []types.Link
	err := c.db.Select(&lxs, "SELECT * FROM links WHERE from_id = ?", ID)

	if err != nil {
		panic(err)
		return lxs, false
	}
	return lxs, true
}

func (c *Client) GetLinksIDMap() map[uuid.UUID][]uuid.UUID {
	lxsByFrom := map[uuid.UUID][]uuid.UUID{}
	rows, _ := c.db.Queryx("SELECT * FROM links")

	for rows.Next() {
		var l types.Link
		err := rows.StructScan(&l)
		if err != nil {
			panic(err)
		}
		ls, ok := lxsByFrom[l.FromID]
		if ok {
			ls = append(ls, l.ToID)
		} else {
			ls = []uuid.UUID{l.ToID}
		}
		lxsByFrom[l.FromID] = ls
	}

	return lxsByFrom
}
func (c *Client) GetEvents() []types.Event {
	var es []types.Event
	err := c.db.Select(&es, "SELECT * FROM events")
	if err != nil {
		panic(err)
	}

	return es
}
