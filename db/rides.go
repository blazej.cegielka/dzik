package db

import (
	"github.com/google/uuid"

	"dzik/types"
)

func (c *Client) GetAvailableRideForShuttle(sID uuid.UUID, space int) (r *types.Ride, err error) {
	var rs []types.Ride
	err = c.db.Select(&rs, "SELECT r.*, COUNT(o.id) AS passengers FROM rides r LEFT JOIN orders o ON r.id = o.ride_id WHERE r.shuttle_id = ? GROUP BY r.id HAVING passengers <= r.capacity - ?", sID, space)
	if err != nil {
		return
	}
	if len(rs) == 0 {
		return
	}

	r = &rs[0]
	return
}

func (c *Client) CreateRide(r types.Ride) {
	sql := `INSERT INTO rides(id, shuttle_id, capacity) VALUES (?, ?, ?)`
	c.db.MustExec(sql, r.ID, r.ShuttleID, r.Capacity)
}
