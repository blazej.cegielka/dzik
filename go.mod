module dzik

go 1.12

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20170425171159-5493cabe49f7
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gocraft/work v0.5.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/robfig/cron v1.2.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
)
