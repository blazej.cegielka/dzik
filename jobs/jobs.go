package jobs

import (
	"github.com/gocraft/work"
	"github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
)

const JobPushOrder = "push-order"
const JobPushRide = "push-ride"
const JobDispatchRide = "dispatch-ride"

func NewRedisPool(host string, port string) *redis.Pool {
	return &redis.Pool{
		MaxActive: 5,
		MaxIdle:   5,
		Wait:      true,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", host+":"+port)
		},
	}
}

func MustPing(pool *redis.Pool) {
	conn := pool.Get()
	defer conn.Close()

	_, err := conn.Do("PING")
	if err != nil {
		panic(err)
	}
}

func MustFlushDB(pool *redis.Pool) {
	conn := pool.Get()
	defer conn.Close()

	_, err := conn.Do("FLUSHDB")
	if err != nil {
		panic(err)
	}
}

type Params work.Q

type Enqueuer interface {
	Enqueue(name string, params Params)
	QPushOrder(shuttleID uuid.UUID, orderID uuid.UUID, passengers int)
}

type enqueuer struct {
	e *work.Enqueuer
}

func NewEnqueuer(namespace string, rp *redis.Pool) Enqueuer {
	e := work.NewEnqueuer(namespace, rp)

	return &enqueuer{e: e}
}

func (je *enqueuer) Enqueue(name string, params Params) {
	// _, err := je.e.Enqueue("send_email", work.Q{"address": "test@example.com", "subject": "hello world", "customer_id": 4})
	_, err := je.e.Enqueue(name, params)
	if err != nil {
		panic(err)
	}
}

func (je *enqueuer) QPushOrder(shuttleID uuid.UUID, orderID uuid.UUID, passengers int) {
	je.Enqueue(JobPushOrder, NewPushOrderParamsMap(shuttleID, orderID, passengers))
}

type PushOrderParams struct {
	ShuttleID  uuid.UUID
	OrderID    uuid.UUID
	Passengers int
}

func NewPushOrderParams(shuttleID uuid.UUID, orderID uuid.UUID, passengers int) PushOrderParams {
	return PushOrderParams{shuttleID, orderID, passengers}
}

func NewPushOrderParamsMap(shuttleID uuid.UUID, orderID uuid.UUID, passengers int) Params {
	return Params{
		"shuttleID":  shuttleID,
		"orderID":    orderID,
		"passengers": passengers,
	}
}

func NewPushOrderParamsFromJob(job *work.Job) (pop PushOrderParams, e error) {
	shuttleID := uuid.MustParse(job.ArgString("shuttleID"))
	orderID := uuid.MustParse(job.ArgString("orderID"))
	passengers := int(job.ArgInt64("passengers"))

	if err := job.ArgError(); err != nil {
		e = err
		return
	}

	return NewPushOrderParams(shuttleID, orderID, passengers), nil
}

type mockCall struct {
	Name string
	// params work.Q
	Params Params
}

type EnqueuerMock struct {
	Calls []mockCall
}

func NewEnqueuerMock() Enqueuer {
	return &EnqueuerMock{}
}

func (je *EnqueuerMock) Enqueue(name string, params Params) {
	je.Calls = append(je.Calls, mockCall{name, params})
}

func (je *EnqueuerMock) QPushOrder(shuttleID uuid.UUID, orderID uuid.UUID, passengers int) {
	je.Enqueue(JobPushOrder, NewPushOrderParamsMap(shuttleID, orderID, passengers))
}

func NewClient(namespace string, rp *redis.Pool) *work.Client {
	return work.NewClient(namespace, rp)
}

type Processor interface {
	PushOrder(job *work.Job) error
}

type Worker interface {
	Start()
	Stop()
}

type worker struct {
	pool *work.WorkerPool
}

func NewWorker(namespace string, rp *redis.Pool, processor Processor) Worker {
	// Make a new pool. Arguments:
	// Context{} is a struct that will be the context for the request.
	// 10 is the max concurrency
	// namespace is the Redis namespace

	pool := work.NewWorkerPool(struct{}{}, 10, namespace, rp)

	// Map the name of jobs to handler functions
	pool.Job(JobPushOrder, processor.PushOrder)

	return &worker{pool: pool}
}

func (w *worker) Start() {
	w.pool.Start()
}

func (w *worker) Stop() {
	w.pool.Stop()
}
