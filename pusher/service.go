package pusher

import (
	"log"

	"github.com/google/uuid"

	"dzik/jobs"
	"dzik/repository"
	"dzik/utils"
)

// Service provides pushing operations.
type Service interface {
	PushOrder(shuttleID uuid.UUID, orderID uuid.UUID, passengers int)
	PushRide(shuttleID uuid.UUID, rideID uuid.UUID, capacity int)
}

// service is a concrete implementation of SearchService
type service struct {
	db repository.Repository
	je jobs.Enqueuer
}

func NewService(db repository.Repository, je jobs.Enqueuer) Service {
	return &service{
		db: db,
		je: je,
	}
}

func (sv *service) PushOrder(shuttleID uuid.UUID, orderID uuid.UUID, passengers int) {
	r, err := sv.db.GetAvailableRideForShuttle(shuttleID, passengers)
	if err != nil {
		log.Printf("Error getting available ride: %s", err)
		return
	}
	if r != nil {
		log.Printf("Ride is available")
		sv.db.UpdateOrderRide(orderID, r.ID)
		return
	}

	os, err := sv.db.GetOrdersWithoutRideByShuttleID(shuttleID)
	if err != nil {
		log.Printf("Error getting orders without ride: %s", err)
	}
	log.Printf("orders %v", os)

	pos := utils.PackOrders(os)

	if len(pos) == 0 {
		return
	}

	sv.je.Enqueue(jobs.JobDispatchRide, jobs.Params{"who": "pusher"})
}

func (sv *service) PushRide(shuttleID uuid.UUID, rideID uuid.UUID, capacity int) {
	os, err := sv.db.GetOrdersWithoutRideByShuttleID(shuttleID)
	if err != nil {
		log.Printf("Error getting orders without ride: %s", err)
	}
	pos := utils.PackOrders(os)
	assignOrders := utils.AssignRideToPack(pos, capacity)

	log.Printf("assignOrders %v", assignOrders)
	for _, o := range assignOrders {
		sv.db.UpdateOrderRide(o.ID, rideID)
	}
}
