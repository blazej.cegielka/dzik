package pusher

import (
	"os"
	"testing"

	"dzik/jobs"
	ltesting "dzik/testing"
	"dzik/types"
)

var (
	sv Service
)

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	defer ltesting.TestMain()()
	sv = NewService(ltesting.DB, ltesting.JE)

	return m.Run()
}

func TestPushOrderDispatchNewRide(t *testing.T) {
	defer ltesting.Setup()()

	s := types.Shuttle{}
	ltesting.DB.CreateShuttle(s)
	o := types.Order{Passengers: 1, Split: 1}
	ltesting.DB.CreateOrder(o)

	sv.PushOrder(s.ID, o.ID, o.Passengers)
	calls := ltesting.JE.(*jobs.EnqueuerMock).Calls
	expJobsNo := 1
	if len(calls) != expJobsNo {
		t.Errorf("Expects new jobs: %d, got %d", expJobsNo, len(calls))
	}

	expJobName := jobs.JobDispatchRide
	if calls[0].Name != expJobName {
		t.Errorf("Expects job.name: %s, got %s", expJobName, calls[0].Name)
	}

	expJobParams := "pusher"
	if calls[0].Params["who"] != expJobParams {
		t.Errorf("Expects job.params: %s, got %s", expJobParams, calls[0].Params)
	}
}

func TestPushOrderNoRideButNoDispatch(t *testing.T) {
	defer ltesting.Setup()()

	s := types.Shuttle{}
	ltesting.DB.CreateShuttle(s)
	o := types.Order{Passengers: 1, Split: 2}
	ltesting.DB.CreateOrder(o)

	sv.PushOrder(s.ID, o.ID, o.Passengers)
	calls := ltesting.JE.(*jobs.EnqueuerMock).Calls
	expJobsNo := 0
	if len(calls) != expJobsNo {
		t.Errorf("Expects new jobs: %d, got %d", expJobsNo, len(calls))
	}
}

func TestPushOrderRideExists(t *testing.T) {
	defer ltesting.Setup()()

	s := types.Shuttle{}
	ltesting.DB.CreateShuttle(s)
	o := types.Order{Passengers: 1, Split: 2}
	ltesting.DB.CreateOrder(o)
	r := types.Ride{Capacity: 1}
	ltesting.DB.CreateRide(r)

	sv.PushOrder(s.ID, o.ID, o.Passengers)
	calls := ltesting.JE.(*jobs.EnqueuerMock).Calls
	expJobsNo := 0
	if len(calls) != expJobsNo {
		t.Errorf("Expects new jobs: %d, got %d", expJobsNo, len(calls))
	}

	os := ltesting.DB.GetOrders(o.UserID)
	rideID := os[0].RideID
	if rideID == nil || *rideID != r.ID {
		t.Errorf("Expects order.RideID: %d, got %v", r.ID, rideID)
	}
}

func TestPushRide(t *testing.T) {
	defer ltesting.Setup()()

	s := types.Shuttle{}
	ltesting.DB.CreateShuttle(s)
	o := types.Order{}
	ltesting.DB.CreateOrder(o)
	r := types.Ride{Capacity: 1}
	ltesting.DB.CreateRide(r)

	sv.PushRide(s.ID, r.ID, r.Capacity)

	os := ltesting.DB.GetOrders(o.UserID)
	rideID := os[0].RideID
	if rideID == nil || *rideID != r.ID {
		t.Errorf("Expects order.RideID: %d, got %v", r.ID, rideID)
	}
}
