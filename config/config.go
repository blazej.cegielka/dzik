package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Specification struct {
	DBName        string `envconfig:"db_name" default:"db/foo.db"`
	DBSchema      string `envconfig:"db_schema" default:"db/schema.sql"`
	RedisHost     string `split_words:"true"`
	RedisPort     string `split_words:"true" default:"6379"`
	JobsNamespace string `split_words:"true" default:"orders"`
	ServerPort    string `split_words:"true" default:"5010"`
}

func NewConfig() Specification {
	var s Specification

	if err := envconfig.Process("mruk", &s); err != nil {
		panic(err)
	}

	return s
}
