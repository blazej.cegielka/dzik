package planner

import (
	"fmt"

	"github.com/google/uuid"

	"dzik/jobs"
	"dzik/repository"
	"dzik/types"
)

// Service provides planning operations.
type Service interface {
	ListLocations() []types.Location
	ListLinks() map[uuid.UUID][]uuid.UUID
	ListShuttles(fromID uuid.UUID, toID uuid.UUID, date string) []types.Shuttle

	CreateOrder(userID string, shuttle types.Shuttle, passengers int, split int) (*types.Order, error)
	ListOrders(userID string) []types.Order
}

// service is a concrete implementation of Service
type service struct {
	db repository.Repository
	je jobs.Enqueuer
}

func NewService(db repository.Repository, je jobs.Enqueuer) Service {
	return &service{
		db: db,
		je: je,
	}
}

func (sv *service) ListLocations() []types.Location {
	return sv.db.GetLocations()
}

func (sv *service) ListLinks() map[uuid.UUID][]uuid.UUID {
	return sv.db.GetLinksIDMap()
}

func (sv *service) ListShuttles(fromID uuid.UUID, toID uuid.UUID, date string) []types.Shuttle {
	shs := sv.getShuttlesFromEvents(date)
	shs = sv.filterShuttles(shs, func(sh types.Shuttle) bool {
		return (sh.FromID == fromID && sh.ToID == toID)
	})
	shs = sv.enhanceShuttles(shs)

	return shs
}

func (sv *service) getShuttlesFromEvents(date string) []types.Shuttle {
	shs := []types.Shuttle{}
	for _, e := range sv.db.GetEvents() {
		lxs, _ := sv.db.GetLocationLinks(e.LocationID)
		for _, lx := range lxs {
			sh := types.NewShuttle(lx.FromID, lx.ToID, date, e.Time)
			shs = append(shs, sh)
		}
	}
	return shs
}

func (sv *service) filterShuttles(shs []types.Shuttle, f func(types.Shuttle) bool) []types.Shuttle {
	fshs := make([]types.Shuttle, 0)
	for _, sh := range shs {
		if f(sh) {
			fshs = append(fshs, sh)
		}
	}
	return fshs
}

func (sv *service) enhanceShuttles(shs []types.Shuttle) []types.Shuttle {
	for i, sh := range shs {
		if v, ok := sv.db.GetShuttleByIDWithOrders(sh.ID); ok {
			sh = v
		} else {
			if v, ok := sv.db.GetLocationByID(sh.FromID); ok {
				sh.FromName = v.Name
			}
			if v, ok := sv.db.GetLocationByID(sh.ToID); ok {
				sh.ToName = v.Name
			}
		}
		shs[i] = sh
	}
	return shs
}

func (sv *service) CreateOrder(userID string, shuttle types.Shuttle, passengers int, split int) (o *types.Order, err error) {
	s, ok := sv.db.GetShuttleByID(shuttle.ID)
	if !ok {
		// TODO: check if such shuttle is possible
		s = types.NewShuttle(shuttle.FromID, shuttle.ToID, shuttle.Date, shuttle.Departure)
		if s.ID != shuttle.ID {
			err = fmt.Errorf("Generated shuttle.ID `%s` differs from ordered: %s", s.ID, shuttle.ID)
			return
		}

		sv.db.CreateShuttle(s)
	}

	o = types.NewOrder(userID, s, passengers, split)
	sv.db.CreateOrder(*o)

	sv.je.QPushOrder(s.ID, o.ID, passengers)
	return
}

func (sv *service) ListOrders(userID string) []types.Order {
	return sv.db.GetOrders(userID)
}
