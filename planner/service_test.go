package planner

import (
	"os"
	"testing"

	"fmt"

	"github.com/google/uuid"

	"dzik/jobs"
	ltesting "dzik/testing"
	"dzik/types"
)

var locNewquay = uuid.MustParse("00000000-0000-0000-0000-000000000000")
var locTruro = uuid.MustParse("00000000-0000-0000-0000-000000000001")
var locFal = uuid.MustParse("00000000-0000-0000-0000-000000000002")

const user1 = "foo@mruk.io"
const user2 = "bar@mruk.io"

var (
	sv Service
)

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	defer ltesting.TestMain()()
	sv = NewService(ltesting.DB, ltesting.JE)

	return m.Run()
}

func TestFindShuttles(t *testing.T) {
	defer ltesting.Setup()()

	events := ltesting.DB.GetEvents()
	eventsTimeCnt := len(events[0].Time)
	expEventsTimeCnt := 5
	if eventsTimeCnt != expEventsTimeCnt {
		t.Errorf("event doesn't have %d times, got %d", expEventsTimeCnt, eventsTimeCnt)
	}

	type shuttleOpt struct {
		from uuid.UUID
		to   uuid.UUID
		date string
	}

	shuttleTests := []shuttleOpt{
		{locNewquay, locFal, "2019-05-01"},
		{locNewquay, locTruro, "2019-05-01"},
	}

	for _, st := range shuttleTests {
		t.Run(fmt.Sprintf("%v", st), func(t *testing.T) {
			got := sv.ListShuttles(st.from, st.to, st.date)

			if len(got) != expEventsTimeCnt {
				t.Errorf("Shuttles don't match #event.Time(%d), got %d", expEventsTimeCnt, len(got))
			}

			for _, s := range got {
				if s.FromID != st.from {
					t.Errorf("Shuttle.FromID is not `%d`, got `%d`", st.from, s.FromID)
				}
				if s.ToID != st.to {
					t.Errorf("Shuttle.ToID is not `%d`, got `%d`", st.to, s.ToID)
				}
				if s.Date != st.date {
					t.Errorf("Shuttle.Date is not `%s`, got `%s`", st.date, s.Date)
				}
				// if s.Departure != events[0].Time[i] {
				// 	t.Errorf("Shuttle.Departure is not event.Time `%s`, got `%s`", events[0].Time[i], s.Departure)
				// }
				// if s.Arrival != events[0].Time[i] {
				// 	t.Errorf("Shuttle.Arrival is not event.Time `%s`, got `%s`", events[0].Time[i], s.Arrival)
				// }

				expUUID := types.NewUUIDFromShuttle(s)
				if s.ID != expUUID {
					t.Errorf("Shuttle.ID is not `%v`, got `%v`", expUUID, s.ID)
				}
			}
		})
	}
}

func TestOrderShuttle(t *testing.T) {
	defer ltesting.Setup()()
	cx := jobs.NewClient(ltesting.Config.JobsNamespace, ltesting.JobsStore)

	type orderIn struct {
		user string
		from uuid.UUID
		to   uuid.UUID
		date string
		time string
	}
	type orderOut struct {
		popularity int
	}

	var orderTests = []struct {
		in  orderIn
		out orderOut
	}{
		{
			orderIn{user1, locNewquay, locFal, "2019-05-01", "10:35"},
			orderOut{popularity: 1},
		},
		{
			orderIn{user1, locNewquay, locFal, "2019-05-01", "10:35"},
			orderOut{popularity: 2},
		},
		{
			orderIn{user1, locNewquay, locFal, "2019-05-02", "10:35"},
			orderOut{popularity: 1},
		},
		{
			orderIn{user1, locNewquay, locFal, "2019-05-02", "10:35"},
			orderOut{popularity: 2},
		},
	}

	orders := sv.ListOrders(user1)
	ordersCnt := len(orders)
	expOrdersCnt := 0
	if ordersCnt != expOrdersCnt {
		t.Errorf("Expects %d orders, got %d", expOrdersCnt, ordersCnt)
	}

	for _, ot := range orderTests {
		t.Run("Existing Shuttle", func(t *testing.T) {
			sx := types.NewShuttle(ot.in.from, ot.in.to, ot.in.date, ot.in.time)
			o, _ := sv.CreateOrder(ot.in.user, sx, 1, 2)
			s, ok := ltesting.DB.GetShuttleByIDWithOrders(sx.ID)
			if !ok {
				t.Errorf("Shuttle not found: %s", sx.ID)
			}
			if s.Popularity != ot.out.popularity {
				t.Errorf("Stored Shuttle.Popularity should increase. Expected `%d`, got `%d`", ot.out.popularity, s.Popularity)
			}
			if o.ID == (uuid.UUID{}) {
				t.Errorf("Expected non zero order id")
			}

			qs, _ := cx.Queues()
			for _, q := range qs {
				fmt.Printf("queue: %v, %d", q.JobName, q.Count)
			}
		})
	}
	orders = sv.ListOrders(user1)
	ordersCnt = len(orders)
	expOrdersCnt += len(orderTests)
	if ordersCnt != expOrdersCnt {
		t.Errorf("Expects %d orders, got %d", expOrdersCnt, ordersCnt)
	}
}

func TestListOrders(t *testing.T) {
	defer ltesting.Setup()()

	type testIn struct {
		user string
		from uuid.UUID
		to   uuid.UUID
		date string
		time string
	}
	type testOut struct {
		count int
	}

	var testCases = []struct {
		in  testIn
		out testOut
	}{
		{
			testIn{user1, locNewquay, locFal, "2019-05-01", "10:35"},
			testOut{count: 1},
		},
		{
			testIn{user2, locNewquay, locFal, "2019-05-01", "10:35"},
			testOut{count: 1},
		},
		{
			testIn{user1, locNewquay, locFal, "2019-05-01", "10:35"},
			testOut{count: 2},
		},
	}

	for _, tc := range testCases {
		t.Run("ListOrders", func(t *testing.T) {
			sx := types.NewShuttle(tc.in.from, tc.in.to, tc.in.date, tc.in.time)
			_, err := sv.CreateOrder(tc.in.user, sx, 1, 2)
			if err != nil {
				t.Errorf("Error creating order: %s", err)
			}

			orders := sv.ListOrders(user1)
			ordersCnt := len(orders)
			if ordersCnt != tc.out.count {
				t.Errorf("Expects %d orders, got %d", tc.out.count, ordersCnt)
			}
		})
	}
}
