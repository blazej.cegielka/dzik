package server

import (
	"testing"

	// "log"
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	ltesting "dzik/testing"
)

func TestMain(m *testing.M) {
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	defer ltesting.TestMain()()
	return m.Run()
}

func TestCors(t *testing.T) {
	sv := NewServer("", nil)

	req, _ := http.NewRequest("GET", "http://example.com/home", nil)
	rec := httptest.NewRecorder()

	sv.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Errorf("rec.Code = %d; want = %d", rec.Code, http.StatusOK)
	}

	t.Run(fmt.Sprintf("Access-Control-Allow-Origin"), func(t *testing.T) {
		want := "*"
		if got := rec.Header().Get("Access-Control-Allow-Origin"); got != want {
			t.Errorf("Access-Control-Allow-Origin = %q; want = %q", got, want)
		}
	})

	t.Run(fmt.Sprintf("Access-Control-Allow-Headers"), func(t *testing.T) {
		want := "authorization"
		if got := rec.Header().Get("Access-Control-Allow-Headers"); got != want {
			t.Errorf("Access-Control-Allow-Headers = %q; want = %q", got, want)
		}
	})
}

func TestAuth(t *testing.T) {
	e := NewHandler(nil, nil, nil)

	type in struct {
		handler http.Handler
		// ctxKey   contextKey
		ctxKey   string
		ctxValue Token
	}
	type out struct {
		status int
		body   string
	}

	tcs := []struct {
		label string
		in    in
		out   out
	}{
		{
			"no token",
			in{
				handler: NewServer("", nil),
			},
			out{
				status: http.StatusUnauthorized,
				body:   "Required authorization token not found\n",
			},
		},
		{
			"no token when user protected",
			in{
				handler: UserAuthenticatedMiddlewarePartial(http.HandlerFunc(e.Home)),
			},
			out{
				status: http.StatusUnauthorized,
				body:   "no session, login first\n",
			},
		},
		{
			"empty token when user protected",
			in{
				handler:  UserAuthenticatedMiddlewarePartial(http.HandlerFunc(e.Home)),
				ctxKey:   userKey,
				ctxValue: Token{},
			},
			out{
				status: http.StatusOK,
				body:   "Home: /history!",
			},
		},
		{
			"not operator when operator protected",
			in{
				handler:  OperatorMiddlewarePartial(http.HandlerFunc(e.Home)),
				ctxKey:   userKey,
				ctxValue: Token{jwt.Token{Claims: jwt.MapClaims{"https://mruk.io/roles": []interface{}{"a"}}}},
			},
			out{
				status: http.StatusForbidden,
				body:   "not an operator\n",
			},
		},
		{
			"operator when operator protected",
			in{
				handler:  OperatorMiddlewarePartial(http.HandlerFunc(e.Home)),
				ctxKey:   userKey,
				ctxValue: Token{jwt.Token{Claims: jwt.MapClaims{"https://mruk.io/roles": []interface{}{"Operator"}}}},
			},
			out{
				status: http.StatusOK,
				body:   "Home: /history!",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(fmt.Sprintf(tc.label), func(t *testing.T) {
			req, _ := http.NewRequest("GET", "http://example.com/history", nil)
			rec := httptest.NewRecorder()

			if tc.in.ctxKey != "" {
				fmt.Println("key", tc.in.ctxKey)
				ctx := req.Context()
				ctx = context.WithValue(ctx, tc.in.ctxKey, &tc.in.ctxValue.Token)

				req = req.WithContext(ctx)
			}

			tc.in.handler.ServeHTTP(rec, req)

			t.Run(fmt.Sprintf("Status"), func(t *testing.T) {
				want := tc.out.status
				if got := rec.Code; got != want {
					t.Errorf("Unexpected status: %v, want %v", got, want)
				}
			})

			t.Run(fmt.Sprintf("Body"), func(t *testing.T) {
				want := tc.out.body
				if got := rec.Body.String(); got != want {
					t.Errorf("Unexpected body: `%v`, want `%v`", got, want)
				}
			})
		})
	}
}

type header struct {
	key   string
	value string
}

var authorizationHeader = header{
	"Authorization",
	"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UWTFSRE5DT0RWRVFVWTBOamhCTnpnMU1FUTJOVVZEUmpjeU1qSTRNVEJCUWpBNVFUZzRPUSJ9.eyJodHRwczovL21ydWsuaW8vZW1haWwiOiJibGF6ZWouY2VnaWVsa2FAZ21haWwuY29tIiwiaHR0cHM6Ly9tcnVrLmlvL3JvbGVzIjpbIk9wZXJhdG9yIl0sImlzcyI6Imh0dHBzOi8vYnVyYi5ldS5hdXRoMC5jb20vIiwic3ViIjoiZ29vZ2xlLW9hdXRoMnwxMTMwMzYyNTgxMzM3NTkyMTc2NTUiLCJhdWQiOlsiaHR0cDovL2xvY2FsaG9zdDo1MDEwIiwiaHR0cHM6Ly9idXJiLmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1NjkxNzkwNzksImV4cCI6MTU2OTE4NjI3OSwiYXpwIjoiNk5qUHVDT2dPNzh3VzZmT0xfSXBBV2Jub2pyaExiVHAiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0.mV5eeT9m-0-KscAJNb1xXUFXWx43F2R0R6mcfflPq8F6nncNAefuZX2HdFipKpVabDE_6vbdmvRFgPNI9uOJ_a0pE3Gxlxq0wNO14GZIxM7yy7xUgbI6Bg-_E66eoNW83y8AYH3NTDXw63WJ3_rFRmWT5GYlMEw7LU_kb1KGWMBCatzY5iNnAXPgUAXZEmopPY66m7_WQfv4V0ixjQ7NUTtjj63TEX_RE1NQzsnFENOLLAIkacipOunxiIHR3U6rt1LeI1EvUmp7Dc0ATIzbjBQqt7S3vDs0HclMNWXud4vUs-nGklfsCSvCASF5OsqjgruNaNQdXhMHRruFjV8dCA",
}

var authorizationHeaderValidTimeFunc = func() time.Time {
	t, e := time.Parse(time.UnixDate, "Sun Sep 22 21:55:31 BST 2019")
	if e != nil {
		panic(e)
	}
	return t
}

func TestAuth0(t *testing.T) {
	e := NewHandler(nil, nil, nil)
	req, _ := http.NewRequest("GET", "http://example.com/history", nil)
	req.Header.Set(authorizationHeader.key, authorizationHeader.value)

	jwt.TimeFunc = authorizationHeaderValidTimeFunc

	rec := httptest.NewRecorder()
	handler := UserAuthenticatedMiddleware(http.HandlerFunc(e.Home))
	handler.ServeHTTP(rec, req)

	{
		want := http.StatusOK
		if got := rec.Code; got != want {
			t.Errorf("Unexpected status: %v, want %v", got, want)
		}
	}

	{
		want := "Home: /history!"
		if got := rec.Body.String(); got != want {
			t.Errorf("Unexpected body: `%v`, want `%v`", got, want)
		}
	}
}
