package server

import (
	"context"
)

func (h *Handler) OperatorListOrdersWithoutRide(ctx context.Context, r Request) Response {
	return h.di.ListPackedOrders()
}
