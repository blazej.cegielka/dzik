package server

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/google/uuid"
	// "github.com/gorilla/schema"

	"dzik/types"
)

func (h *Handler) ListLocations(ctx context.Context, r Request) Response {
	return h.pl.ListLocations()
}

func (h *Handler) ListLinks(ctx context.Context, r Request) Response {
	return h.pl.ListLinks()
}

type ListShuttlesRequest struct {
	from uuid.UUID
	to   uuid.UUID
	date string
}

func (h *Handler) ListShuttlesRequestDecode(r *http.Request) Request {
	query := r.URL.Query()
	qfrom := query.Get("from")
	qto := query.Get("to")
	qdate := query.Get("date")

	from, err := uuid.Parse(qfrom)
	if err != nil {
		fmt.Printf("From is not uuid: %s: '%s'\n", err, qfrom)
	}
	to, err := uuid.Parse(qto)
	if err != nil {
		fmt.Printf("To is not uuid: %s: '%s'\n", err, qfrom)
	}

	return ListShuttlesRequest{
		from: from,
		to:   to,
		date: qdate,
	}
}

func (h *Handler) ListShuttles(ctx context.Context, r Request) Response {
	rr := r.(ListShuttlesRequest)
	return h.pl.ListShuttles(rr.from, rr.to, rr.date)
}

type CreateOrderRequest struct {
	from       uuid.UUID
	to         uuid.UUID
	date       string
	time       string
	id         uuid.UUID
	passengers int
	split      int
}

func (h *Handler) CreateOrderRequestDecode(r *http.Request) Request {
	query := r.URL.Query()
	qfrom := query.Get("from")
	qto := query.Get("to")
	qdate := query.Get("date")
	qtime := query.Get("time")
	qid := query.Get("id")
	qpassengers := query.Get("passengers")
	qsplit := query.Get("split")

	from, err := uuid.Parse(qfrom)
	if err != nil {
		fmt.Printf("From is not uuid: %s: '%s'\n", err, qfrom)
	}
	to, err := uuid.Parse(qto)
	if err != nil {
		fmt.Printf("To is not uuid: %s: '%s'\n", err, qfrom)
	}
	id, err := uuid.Parse(qid)
	if err != nil {
		fmt.Printf("Error parsing UUID: %s: %s\n", err, qid)
	}

	passengers, err := strconv.Atoi(qpassengers)
	if err != nil {
		fmt.Printf("Passengers is not int\n")
	}

	split, err := strconv.Atoi(qsplit)
	if err != nil {
		fmt.Printf("Split is not int\n")
	}

	return CreateOrderRequest{
		from:       from,
		to:         to,
		id:         id,
		date:       qdate,
		time:       qtime,
		passengers: passengers,
		split:      split,
	}
}

func (h *Handler) CreateOrder(ctx context.Context, r Request) Response {
	rr := r.(CreateOrderRequest)

	token := NewTokenFromContext(ctx)
	email := token.Email()
	fmt.Printf("This is an authenticated request: %v\n", email)

	s := types.NewShuttle(rr.from, rr.to, rr.date, rr.time)
	o, err := h.pl.CreateOrder(email, s, rr.passengers, rr.split)
	if err != nil {
		fmt.Printf("Error ordering shuttle: %s\n", err)
	}

	return o
}

func (h *Handler) ListOrders(ctx context.Context, r Request) Response {
	token := NewTokenFromContext(ctx)
	email := token.Email()

	return h.pl.ListOrders(email)
}
