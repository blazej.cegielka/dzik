package server

import (
	"fmt"
	"net/http"

	"dzik/dispatcher"
	"dzik/planner"
	"dzik/pusher"
)

type Handler struct {
	pl planner.Service
	pu pusher.Service
	di dispatcher.Service
}

type Request interface{}
type Response interface{}

func NewHandler(pl planner.Service, pu pusher.Service, di dispatcher.Service) *Handler {
	return &Handler{pl, pu, di}
}

func (h *Handler) Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Home: %s!", r.URL.Path)
}
