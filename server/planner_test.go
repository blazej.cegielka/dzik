package server

import (
	"testing"

	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"

	"dzik/planner"
	ltesting "dzik/testing"
	"dzik/types"
)

func TestOrder(t *testing.T) {
	ro := CreateOrderRequest{
		from:       uuid.MustParse("00000000-0000-0000-0000-000000000000"),
		to:         uuid.MustParse("00000000-0000-0000-0000-000000000002"),
		date:       "2019-10-01",
		time:       "10:35",
		id:         uuid.MustParse("52b9f30c-bfa0-5812-b460-77677713b2f3"),
		passengers: 1,
		split:      2,
	}
	ruser := "Operator"

	pl := planner.NewService(ltesting.DB, ltesting.JE)
	e := NewHandler(pl, nil, nil)

	lurl := "http://example.com/order"
	q := url.Values{
		"from":       []string{ro.from.String()},
		"to":         []string{ro.to.String()},
		"date":       []string{ro.date},
		"time":       []string{ro.time},
		"id":         []string{ro.id.String()},
		"passengers": []string{strconv.Itoa(ro.passengers)},
		"split":      []string{strconv.Itoa(ro.split)},
	}

	req, _ := http.NewRequest("GET", lurl, nil)
	req.URL.RawQuery = q.Encode()
	rec := httptest.NewRecorder()

	ctx := req.Context()
	ctx = context.WithValue(ctx, userKey, &jwt.Token{Claims: jwt.MapClaims{"https://mruk.io/email": ruser}})
	req = req.WithContext(ctx)

	handler := makeHandler(e.CreateOrder, e.CreateOrderRequestDecode, jsonEncoder)
	handler.ServeHTTP(rec, req)

	{
		want := http.StatusOK
		if got := rec.Code; got != want {
			t.Errorf("Unexpected status: %v, want %v", got, want)
		}
	}

	{
		var order types.Order
		if err := json.Unmarshal(rec.Body.Bytes(), &order); err != nil {
			panic(err)
		}

		fmt.Println(order)

		if order.ShuttleID != ro.id {
			t.Errorf("Unexpected Id: %v, want %v", order.ShuttleID, ro.id)
		}
		if order.UserID != ruser {
			t.Errorf("Unexpected Id: %v, want %v", order.UserID, ruser)
		}
		if order.ShuttleDate != ro.date {
			t.Errorf("Unexpected Id: %v, want %v", order.ShuttleDate, ro.date)
		}
		if order.ShuttleDeparture != ro.time {
			t.Errorf("Unexpected Id: %v, want %v", order.ShuttleDeparture, ro.time)
		}
		if order.Passengers != ro.passengers {
			t.Errorf("Unexpected Id: %v, want %v", order.Passengers, ro.passengers)
		}
		if order.Split != ro.split {
			t.Errorf("Unexpected Id: %v, want %v", order.Split, ro.split)
		}
	}
}

func TestShuttle(t *testing.T) {
	pl := planner.NewService(ltesting.DB, ltesting.JE)
	e := NewHandler(pl, nil, nil)
	req, _ := http.NewRequest("GET", "http://example.com/shuttles", nil)
	rec := httptest.NewRecorder()

	handler := makeHandler(e.ListShuttles, e.ListShuttlesRequestDecode, jsonEncoder)
	handler.ServeHTTP(rec, req)

	{
		want := http.StatusOK
		if got := rec.Code; got != want {
			t.Errorf("Unexpected status: %v, want %v", got, want)
		}
	}
}
