package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
)

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

func CreateJwtMiddleware() *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			// Verify 'aud' claim
			aud := "http://localhost:5010"
			checkAud := token.Claims.(jwt.MapClaims).VerifyAudience(aud, false)
			if !checkAud {
				return token, errors.New("Invalid audience")
			}
			// Verify 'iss' claim
			iss := "https://burb.eu.auth0.com/"
			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(iss, false)
			if !checkIss {
				return token, errors.New("Invalid issuer")
			}

			cert, err := getPemCert(token)
			if err != nil {
				panic(err.Error())
			}

			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil
		},
		SigningMethod: jwt.SigningMethodRS256,
		// Debug:         true,
	})
}

var jwtMiddleware = CreateJwtMiddleware()

func getPemCert(token *jwt.Token) (string, error) {
	cert := ""
	resp, err := http.Get("https://burb.eu.auth0.com/.well-known/jwks.json")

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for k := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("Unable to find appropriate key")
		return cert, err
	}

	return cert, nil
}

type Token struct {
	jwt.Token
}

type contextKey string

// const userKey contextKey = "user"
const userKey string = "user"

func NewTokenFromContext(ctx context.Context) *Token {
	v := ctx.Value(userKey)
	fmt.Println(ctx)
	if v == nil {
		return nil
	}

	return &Token{*v.(*jwt.Token)}
}

func (t *Token) Email() string {
	return t.Claims.(jwt.MapClaims)["https://mruk.io/email"].(string)
}

func (t *Token) Roles() (roles []string) {
	claims := t.Claims.(jwt.MapClaims)["https://mruk.io/roles"].([]interface{})
	for _, claim := range claims {
		roles = append(roles, claim.(string))
	}
	return
}

func (t *Token) IsOperator() bool {
	roles := t.Roles()
	log.Printf("This is an authenticated request: %v\n", roles)

	isOperator := false
	for _, role := range roles {
		if role == "Operator" {
			isOperator = true
		}
	}
	return isOperator
}

func UserAuthenticatedMiddleware(next http.Handler) http.Handler {
	return jwtMiddleware.Handler(UserAuthenticatedMiddlewarePartial(next))
}

func OperatorMiddleware(next http.Handler) http.Handler {
	return jwtMiddleware.Handler(OperatorMiddlewarePartial(next))
}

func UserAuthenticatedMiddlewarePartial(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			return
		}

		token := NewTokenFromContext(r.Context())

		if token == nil {
			http.Error(w, "no session, login first", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func OperatorMiddlewarePartial(next http.Handler) http.Handler {
	return UserAuthenticatedMiddlewarePartial(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := NewTokenFromContext(r.Context())
		isOperator := token.IsOperator()

		if !isOperator {
			http.Error(w, "not an operator", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	}))
}
