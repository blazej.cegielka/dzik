package server

import (
	"context"
	"encoding/json"
	"net/http"
)

type Server struct {
	port string
	mux  *http.ServeMux
}

func NewServer(port string, h *Handler) *Server {
	mux := http.NewServeMux()

	// guest
	mux.HandleFunc("/", h.Home)
	mux.HandleFunc("/locations", makeHandleFunc(h.ListLocations, nil, jsonEncoder))
	mux.HandleFunc("/links", makeHandleFunc(h.ListLinks, nil, jsonEncoder))
	mux.HandleFunc("/shuttles", makeHandleFunc(h.ListShuttles, h.ListShuttlesRequestDecode, jsonEncoder))

	// logged in user
	mux.Handle("/order", UserAuthenticatedMiddleware(
		makeHandler(h.CreateOrder, h.CreateOrderRequestDecode, jsonEncoder)))
	mux.Handle("/history", UserAuthenticatedMiddleware(
		makeHandler(h.ListOrders, nil, jsonEncoder)))

	// operator
	mux.Handle("/oporders", OperatorMiddleware(
		makeHandler(h.OperatorListOrdersWithoutRide, nil, jsonEncoder)))

	return &Server{
		mux:  mux,
		port: port,
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	corsMiddleware(s.mux).ServeHTTP(w, r)
}

func (s *Server) Run() {
	panic(http.ListenAndServe(":"+s.port, s))
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "authorization")

		next.ServeHTTP(w, r)
	})
}

func makeHandleFunc(endpoint func(context.Context, Request) Response, decode func(*http.Request) Request, encode func(http.ResponseWriter, Response)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var dr Request
		if decode != nil {
			dr = decode(r)
		} else {
			dr = Request(nil)
		}
		encode(w, endpoint(r.Context(), dr))
	}
}

func makeHandler(endpoint func(context.Context, Request) Response, decode func(*http.Request) Request, encode func(http.ResponseWriter, Response)) http.Handler {
	return http.HandlerFunc(makeHandleFunc(endpoint, decode, encode))
}

func jsonEncoder(w http.ResponseWriter, response Response) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		panic(err)
	}
}
